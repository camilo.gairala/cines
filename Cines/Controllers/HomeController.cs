﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Cines.DAL;
using Cines.Filtros;
using Cines.Models;

namespace Cines.Controllers
{
    public class HomeController : Controller
    {

        private CineContext db = new CineContext();

        public ActionResult Index()
        {
            return RedirectToAction("Index", "Peliculas");
        }

        public ActionResult Login()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Login(String Username, String Password)
        {
            var user = db.Usuarios.Where(u => u.Username == Username).ToList();
            if(user.Count() == 0)
            {
                ViewBag.MensajeError = "Usuario inexistente";
                return View();
            }
            if(user.First().Password != Password)
            {
                ViewBag.MensajeError = "Contraseña incorrecta";
                return View();
            }
            Session["Usuario"] = user.First();
            return RedirectToAction("Index");
        }

        [AuthFilter]
        public ActionResult Logout()
        {
            Session["Usuario"] = null;
            return RedirectToAction("Index");
        }
    }
}