﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Cines.DAL;
using Cines.Filtros;
using Cines.Models;

namespace Cines.Controllers
{
    public class PeliculaComercialController : Controller
    {
        private CineContext db = new CineContext();

        // GET: PeliculaComercial/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var pel = db.Peliculas.Find(id);
            var tipo = new PeliculaComercial();


            
            if(pel==null || pel.GetType() != tipo.GetType())
            {
                ViewBag.MensajeError = "La pelicula ingresada no pertenece a la categoria o no existe";
                return View("ErrorPelicula");
            }

            PeliculaComercial peliculaComercial = (PeliculaComercial) pel;
            if (peliculaComercial == null)
            {
                return HttpNotFound();
            }

            var funciones = db.Funciones
                .Include(fu => fu.Sala)
                .Where(fu => fu.PeliculaId == id);

            
            var listaFunciones = funciones.ToList();

            ViewBag.funciones = listaFunciones.OrderBy(f => f.Horario);
            return View(peliculaComercial);
        }


        // GET: PeliculaComercial/Create
        [AuthFilter]
        public ActionResult Create()
        {
            return View();
        }

        [AuthFilter]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(PeliculaComercial peliculaComercial,List<String> Actores)
        {
            if (peliculaComercial.DuracionMinutos < 1 || peliculaComercial.DuracionMinutos > 360)
            {
                ViewBag.MensajeError = "La duracion debe estar entre 1-360 minutos";
                return View("ErrorPelicula");
            }

            var name = DateTime.Now.ToString("yyyyMMddHHmmss");

            peliculaComercial.ImageUrl = "~/Recursos/Imagenes/" + name + peliculaComercial.Imagen.FileName;
            peliculaComercial.Imagen.SaveAs(Server.MapPath("~/Recursos/Imagenes") + "/" + name + peliculaComercial.Imagen.FileName);

            //Se que esto no es lo ideal, se que es poco seguro, pero no llegué a tiempo de desarrollar nada más apto que cubriera:
            //La cantidad de actores que se pudieran ingresar, el máximo de caracteres por nombre, que haya uno como mínimo, etc

            Actores.RemoveAll(x => string.IsNullOrEmpty(x));
            

            string actoresTodos = string.Join(", ", Actores);
            peliculaComercial.Actores = actoresTodos;

            if (ModelState.IsValid)
            {
                db.Peliculas.Add(peliculaComercial);
                db.SaveChanges();
                return RedirectToAction("Index", "Peliculas");
            }

            return View(peliculaComercial);
        }

        [AuthFilter]
        // GET: PeliculaComercial/Delete/5
        public ActionResult Delete(int? id)
        {
            var pel = db.Peliculas.Find(id);
            var tipo = new PeliculaComercial();

            if (pel == null || pel.GetType() != tipo.GetType())
            {
                ViewBag.MensajeError = "La pelicula ingresada no pertenece a la categoria o no existe";
                return View("ErrorPelicula");
            }

            PeliculaComercial peliculaComercial = (PeliculaComercial)pel;
            if (peliculaComercial == null)
            {
                return HttpNotFound();
            }

            //Se que se repite, pero es para agregar más seguridad
            var funciones = db.Funciones.ToList();

            foreach (var f in funciones)
            {
                if (f.PeliculaId == id)
                {
                    ViewBag.MensajeError = "La pelicula esta en cartelera, por lo que no puede eliminarse";
                    return View("ErrorPelicula");
                }
            }

            return View(peliculaComercial);
        }

        [AuthFilter]
        // POST: PeliculaComercial/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            var pel = db.Peliculas.Find(id);
            var tipo = new PeliculaComercial();

            if (pel == null || pel.GetType() != tipo.GetType())
            {
                ViewBag.MensajeError = "La pelicula ingresada no pertenece a la categoria o no existe";
                return View("ErrorPelicula");
            }

            PeliculaComercial peliculaComercial = (PeliculaComercial) pel;
            String UrlImagen =  peliculaComercial.ImageUrl;


             //Asumo que hay que eliminar la imagen. Si se quisiera mantener, quizás porque varias películas pudieran llegar a repetir, se puede poner que busque 
             //en la base y solo elimine si ningúna otra tiene ese path en su campo, o que si está repetida se guarde como nombre2
            
            System.IO.File.Delete(Server.MapPath(UrlImagen));

            db.Peliculas.Remove(peliculaComercial);
            db.SaveChanges();
            return RedirectToAction("Index","Peliculas");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
