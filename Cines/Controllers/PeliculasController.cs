﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Cines.DAL;
using Cines.Filtros;
using Cines.Models;

namespace Cines.Controllers
{
    public class PeliculasController : Controller
    {
        private CineContext db = new CineContext();

        // GET: Peliculas
        public ActionResult Index()
        {
            var peliculas = db.Peliculas.ToList().OrderBy(Pelicula => Pelicula.Nombre);
            ViewBag.Cines = db.Cines.ToList();

            return View(peliculas.ToList());
        }

        // GET: Peliculas/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var comerciales = db.Peliculas.OfType<PeliculaComercial>();
            var documentales = db.Peliculas.OfType<PeliculaDocumental>();

            foreach (var pel in comerciales)
            {
                if (pel.ID == id)
                {
                    return RedirectToAction("Details", "PeliculaComercial", new { id });
                }
            }
            foreach (var pel in documentales)
            {
                if (pel.ID == id)
                {
                    return RedirectToAction("Details", "PeliculaDocumental", new { id });
                }
            }
            ViewBag.MensajeError = "Película no encontrada";
            return View("ErrorPelicula");
        }

        // GET: Peliculas/Create
        [AuthFilter]
        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        [AuthFilter]
        public ActionResult CreateComercial()
        {
            return RedirectToAction("Create", "PeliculaComercial");
        }

        [HttpPost]
        public ActionResult CreateDocumental()
        {
            return RedirectToAction("Create","PeliculaDocumental");
        }


        [AuthFilter]
        public ActionResult ListaEliminar()
        {
            var peliculas = db.Peliculas.ToList().OrderBy(Pelicula => Pelicula.Nombre);

            return View(peliculas.ToList());
        }

        // GET: Peliculas/Delete/5
        [AuthFilter]
        public ActionResult Delete(int? id)
        {   //Si quiero eliminar cada una
            
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            /*
            Pelicula pelicula = db.Peliculas.Find(id);
            if (pelicula == null)
            {
                return HttpNotFound();
            }
            return View(pelicula);*/

            //Esto era por si quería que cada uno eliminara su película


            //CHEQUEAR SI ESTÁ EN EXHIBICIÓN O NO


            var funciones = db.Funciones.ToList();

            foreach (var f in funciones)
            {
                if (f.PeliculaId == id)
                {
                    ViewBag.MensajeError = "La pelicula esta en cartelera, por lo que no puede eliminarse";
                    return View("ErrorPelicula");
                }
            }

            var comerciales = db.Peliculas.OfType<PeliculaComercial>();
            var documentales = db.Peliculas.OfType<PeliculaDocumental>();


            foreach (var pel in comerciales)
            {
                if (pel.ID == id)
                {
                    return RedirectToAction("Delete", "PeliculaComercial", new { id });
                }
            }
            foreach (var pel in documentales)
            {
                if (pel.ID == id)
                {
                    return RedirectToAction("Delete", "PeliculaDocumental", new { id });
                }
                
            }
            ViewBag.MensajeError = "Película no encontrada";
            return View("ErrorPelicula");


        }

        [AuthFilter]
        public ActionResult Administrar()
        {
            return View();
        }

        /*
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Pelicula pelicula = db.Peliculas.Find(id);
            String UrlImagen = pelicula.ImageUrl;
            System.IO.File.Delete(UrlImagen);
            db.Peliculas.Remove(pelicula);
            db.SaveChanges();
            return RedirectToAction("Index");
        }
        */

        public ActionResult Buscar(int cine,String palabra)
        {
            var palabratl = palabra.ToLower();
            if (db.Cines.Find(cine) == null)
            {
                ViewBag.MensajeError = "Cine no encontrado";
                return View("ErrorPelicula");
            }

            ViewBag.Cines = db.Cines.ToList();

            var peliculas = db.Salas
                .Include(sa => sa.Funciones.Select(pe => pe.Pelicula))
                .Where(f => f.CineId == cine);

            var listaPeliculas = new HashSet<Pelicula>();

            foreach(var p in peliculas)
            {
                foreach (var f in p.Funciones)
                {
                    if (f.Pelicula.Nombre.ToLower().Contains(palabratl))
                    {
                        listaPeliculas.Add(f.Pelicula);
                    }
                    //Me doy cuenta de todo lo que está mal en esta parte (romper la abstracción,reutilizar código, etc) pero no encontré la forma de que me trajera los temas
                    //con la película, con lo cual quedaba en null ese campo y no podía utilizarlo desde el model
                    if (f.Pelicula.MyDiscriminator == "PeliculaDocumental")
                    {
                        var pd = (PeliculaDocumental)f.Pelicula;
                        pd.Tema = db.Temas.Find(pd.TemaId);
                        if (pd.Tema.Nombre.ToLower().Contains(palabratl))
                        {
                            listaPeliculas.Add(f.Pelicula);
                        }
                    }

                    if(f.Pelicula.MyDiscriminator == "PeliculaComercial")
                    {
                        var pd = (PeliculaComercial)f.Pelicula;
                        if (pd.Actores.ToLower().Contains(palabratl))
                        {
                            listaPeliculas.Add(f.Pelicula);
                        }
                    }
                }
            }

            return View("Index", listaPeliculas);
        }
         

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
