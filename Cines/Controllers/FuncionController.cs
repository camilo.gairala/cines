﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Cines.DAL;
using Cines.Filtros;
using Cines.Models;

namespace Cines.Controllers
{
    [AuthFilter]
    public class FuncionController : Controller
    {
        private CineContext db = new CineContext();

        public ActionResult Index()
        {
            return View();
        }

        // GET: Funcion
        public ActionResult Lista(int? cine)
        {
            if(cine == null)
            {
                return RedirectToAction("ElegirCine");
            }
            if (db.Cines.Find(cine) == null) {
                ViewBag.MensajeError = "Cine no encontrado";
                return View("ErrorFuncion");
            }
            ViewBag.CineNombre = db.Cines.Find(cine).Nombre;

            var funciones = db.Salas
                .Include(sa => sa.Funciones.Select(pe => pe.Pelicula))
                .Where(f => f.CineId == cine);
            var listaFunciones = funciones.ToList();
            funciones.OrderBy(s => s.Funciones.OrderBy(f=>f.Horario));
            return View(funciones);
        }

        

        // GET: Funcion/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Funcion funcion = db.Funciones.Find(id);
            if (funcion == null)
            {
                return HttpNotFound();
            }
            return View(funcion);
        }
        public ActionResult ElegirCine()
        {
            ViewBag.Cines = db.Cines.ToList();
            return View();
        }

        public ActionResult ElegirCineCrear()
        {
            ViewBag.Cines = db.Cines.ToList();
            return View();
        }

        // GET: Funcion/Create
        public ActionResult Create(int? cine)
        {
            if(cine == null)
            {
                return RedirectToAction("ElegirCineCrear");
            }
            ViewBag.CineNombre = db.Cines.Find(cine).Nombre;
            ViewBag.Salas = db.Salas.Where(s => s.CineId == cine);
            ViewBag.PeliculasComerciales = db.Peliculas.OfType<PeliculaComercial>().ToList();
            ViewBag.PeliculasDocumentales = db.Peliculas.OfType<PeliculaDocumental>().ToList();


            ViewBag.PeliculaId = new SelectList(db.Peliculas, "ID", "Nombre");
            return View();
        }

        

        // POST: Funcion/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Funcion funcion)
        {
            if(db.Peliculas.Find(funcion.PeliculaId).MyDiscriminator == "PeliculaDocumental")
            {
                var salatipo = db.Salas.Find(funcion.SalaId).MyDiscriminator;
                    if(salatipo == "HD") {
                    ViewBag.MensajeError = "Las peliculas documentales no pueden exhibirse en salas HD";
                    return View("ErrorFuncion");
                }
            }

            var dia = funcion.Horario;

            var funcionesDia = db.Funciones.ToList();
            funcionesDia = funcionesDia.Where(f => f.SalaId == funcion.SalaId).ToList();

            var funcionesCargadas = new List<Funcion>();


            foreach(var f in funcionesDia)
            {
                if(f.Horario.Date == dia)
                {
                    funcionesCargadas.Add(f);
                }
                if(f.Horario.Date == dia.AddDays(1) && f.Horario.Hour < 10)
                {
                    funcionesCargadas.Add(f);
                }
            }

            funcionesCargadas = funcionesCargadas.OrderBy(f => f.Horario).ToList();
            //Asumo que como el ejercicio no aclara nada, se pueden crear funciones de días anteriores
            //Sino, sería tan simple como que verifique que el horario sea mayor a now()
            if (!funcionesCargadas.Any() || (funcionesCargadas.Last().Horario.Hour < 10 && funcionesCargadas.Last().Horario.Date == funcion.Horario.Date))
            {
                TimeSpan ts = new TimeSpan(10, 0, 0);
                funcion.Horario = funcion.Horario.Date + ts;
            }
            else
            {
                var ultima = funcionesCargadas.Last();
                var nuevoHorario = ultima.Horario.AddMinutes(db.Peliculas.Find(ultima.PeliculaId).DuracionMinutos);
                nuevoHorario = nuevoHorario.AddMinutes(15);

                TimeSpan ts = new TimeSpan(2, 0, 0);
                var horarioLimite = dia + ts;
                horarioLimite = horarioLimite.AddDays(1);

                if (nuevoHorario < horarioLimite)
                {
                    funcion.Horario = nuevoHorario;
                }
                else
                {
                    ViewBag.MensajeError = "No quedan horarios disponible para esa fecha";
                    return View("ErrorFuncion");
                }

                
            }

            //funcionesDia.Where();
            var cineId = db.Salas.Find(funcion.SalaId).CineId;
            if (ModelState.IsValid)
            {
                db.Funciones.Add(funcion);
                db.SaveChanges();
                return RedirectToAction("Lista",new {cine = cineId });

            }
            ViewBag.PeliculaId = new SelectList(db.Peliculas, "ID", "Nombre", funcion.PeliculaId);
            return RedirectToAction("Lista", new { cine = cineId });
        }


        /*

         // GET: Funcion/Delete/5
         public ActionResult Delete(int? id)
         {
             if (id == null)
             {
                 return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
             }
             Funcion funcion = db.Funciones.Find(id);
             if (funcion == null)
             {
                 return HttpNotFound();
             }
             return View(funcion);
         }

         // POST: Funcion/Delete/5
         [HttpPost, ActionName("Delete")]
         [ValidateAntiForgeryToken]
         public ActionResult DeleteConfirmed(int id)
         {
             Funcion funcion = db.Funciones.Find(id);
             db.Funciones.Remove(funcion);
             db.SaveChanges();
             return RedirectToAction("Lista");
         }*/


        public ActionResult BorrarTodas(int? sala)
        {
            if (sala == null)
            {
                ViewBag.MensajeError = "Sala no ingresada";
                return View("ErrorFuncion");
            }

            var salaAElminar = db.Salas.Find(sala);
            salaAElminar.Funciones = db.Funciones.Where(f => f.SalaId == salaAElminar.Id)
                .Include(fu => fu.Pelicula).ToList();
                //sa.Funciones.Select(pe => pe.Pelicula));

            if (salaAElminar == null)
            {
                ViewBag.MensajeError = "Sala no encontrada";
                return View("ErrorFuncion");
            }
            if (salaAElminar.Funciones == null)
            {
                ViewBag.MensajeError = "La sala no tiene funciones";
                return View("ErrorFuncion");
            }

            return View(salaAElminar);
        }

        [HttpPost, ActionName("BorrarTodas")]
        public ActionResult BorrarTodas(int sala)
        {


            var salaAElminar = db.Salas.Find(sala);

            db.Funciones.RemoveRange(db.Funciones.Where(f => f.SalaId == sala));
            db.SaveChanges();
            return RedirectToAction("Lista", new { cine = salaAElminar.CineId });
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
