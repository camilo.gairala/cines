﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Cines.DAL;
using Cines.Filtros;
using Cines.Models;

namespace Cines.Controllers
{
    public class PeliculaDocumentalController : Controller
    {
        private CineContext db = new CineContext();

        // GET: PeliculaDocumental/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var pel = db.Peliculas.Find(id);
            var tipo = new PeliculaDocumental();

            if (pel == null || pel.GetType() != tipo.GetType())
            {
                ViewBag.MensajeError = "La pelicula ingresada no pertenece a la categoria o no existe";
                return View("ErrorPelicula");
            }

            PeliculaDocumental PeliculaDocumental = (PeliculaDocumental)pel;
            PeliculaDocumental.Tema = db.Temas.Find(PeliculaDocumental.TemaId);
            if (PeliculaDocumental == null)
            {
                return HttpNotFound();
            }

            var funciones = db.Funciones
                .Include(fu => fu.Sala)
                .Where(fu => fu.PeliculaId == id);


            var listaFunciones = funciones.ToList();

            ViewBag.funciones = listaFunciones.OrderBy(f=>f.Horario);

            return View(PeliculaDocumental);
        }

        [AuthFilter]
        public ActionResult Create()
        {
            var temas = db.Temas.ToList();
            ViewBag.Temas = temas;


            return View();
        }

        // POST: PeliculaDocumental/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [AuthFilter]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(PeliculaDocumental PeliculaDocumental)
        {
            if(PeliculaDocumental.DuracionMinutos<1 || PeliculaDocumental.DuracionMinutos > 360)
            {
                ViewBag.MensajeError = "La duracion debe estar entre 1-360 minutos";
                return View("ErrorPelicula");
            }

            var name = DateTime.Now.ToString("yyyyMMddHHmmss");

            PeliculaDocumental.ImageUrl = "~/Recursos/Imagenes/" + name + PeliculaDocumental.Imagen.FileName;
            PeliculaDocumental.Imagen.SaveAs(Server.MapPath("~/Recursos/Imagenes") + "/" + name + PeliculaDocumental.Imagen.FileName);

            if (ModelState.IsValid)
            {
                db.Peliculas.Add(PeliculaDocumental);
                db.SaveChanges();
                return RedirectToAction("Index", "Peliculas");
            }

            return View(PeliculaDocumental);
        }

        // GET: PeliculaDocumental/Delete/5
        [AuthFilter]
        public ActionResult Delete(int? id)
        {
            var pel = db.Peliculas.Find(id);
            var tipo = new PeliculaDocumental();

            if (pel == null || pel.GetType() != tipo.GetType())
            {
                ViewBag.MensajeError = "La pelicula ingresada no pertenece a la categoria o no existe";
                return View("ErrorPelicula");
            }

            PeliculaDocumental PeliculaDocumental = (PeliculaDocumental)pel;
            if (PeliculaDocumental == null)
            {
                return HttpNotFound();
            }

            //Se que se repite, pero es para agregar más seguridad
            var funciones = db.Funciones.ToList();

            foreach (var f in funciones)
            {
                if (f.PeliculaId == id)
                {
                    ViewBag.MensajeError = "La pelicula esta en cartelera, por lo que no puede eliminarse";
                    return View("ErrorPelicula");
                }
            }

            return View(PeliculaDocumental);
        }

        // POST: PeliculaDocumental/Delete/5
        [AuthFilter]
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            var pel = db.Peliculas.Find(id);
            var tipo = new PeliculaDocumental();

            if (pel == null || pel.GetType() != tipo.GetType())
            {
                ViewBag.MensajeError = "La pelicula ingresada no pertenece a la categoria o no existe";
                return View("ErrorPelicula");
            }

            PeliculaDocumental PeliculaDocumental = (PeliculaDocumental)pel;
            String UrlImagen = PeliculaDocumental.ImageUrl;


            //Asumo que hay que eliminar la imagen. Si se quisiera mantener, quizás porque varias películas pudieran llegar a repetir, se puede poner que busque 
            //en la base y solo elimine si ningúna otra tiene ese path en su campo, o que si está repetida se guarde como nombre2

            System.IO.File.Delete(Server.MapPath(UrlImagen));

            db.Peliculas.Remove(PeliculaDocumental);
            db.SaveChanges();
            return RedirectToAction("Index", "Peliculas");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
