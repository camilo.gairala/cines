﻿$(document).ready(function () {
    var max_fields_limit = 10; //No se si hay un máximo posible de actores, por eso puse 10
    var x = 1;
    $('.add_more_button').click(function (e) {
        e.preventDefault();
        if (x < max_fields_limit) {
            x++;
            $('.input_fields_container_part').append('<div><input type="text" required name="Actores"/><a href="#" class="remove_field"  style="margin-left:10px;">Remove</a></div>');
        }
    });
    $('.input_fields_container_part').on("click", ".remove_field", function (e) {
        e.preventDefault(); $(this).parent('div').remove(); x--;
    })
});