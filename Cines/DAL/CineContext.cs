﻿using Cines.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;
using System.Web;

namespace Cines.DAL
{
    public class CineContext : DbContext
    {
        public CineContext() : base("tp2_Gairala")
        {
        }

        public DbSet<Pelicula> Peliculas { get; set; }
        public DbSet<PeliculaComercial> Comercials { get; set; }
        public DbSet<PeliculaDocumental> Documentals { get; set; }

        public DbSet<Sala> Salas { get; set; }

        public DbSet<SalaNormal> SalasNormales { get; set; }
        public DbSet<SalaHD> SalasHD { get; set; }
        public DbSet<Cine> Cines { get; set; }

        public DbSet<Funcion> Funciones { get; set; }

        public DbSet<Tema> Temas { get; set; }

        public DbSet<Usuario> Usuarios { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
        }

    }
}