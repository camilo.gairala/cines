namespace Cines.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class inicial2 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Cine",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Nombre = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Sala",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Numero = c.Int(nullable: false),
                        CineId = c.Int(nullable: false),
                        Discriminator = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Cine", t => t.CineId, cascadeDelete: true)
                .Index(t => t.CineId);
            
            CreateTable(
                "dbo.Funcion",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        PeliculaId = c.Int(nullable: false),
                        SalaId = c.Int(nullable: false),
                        Horario = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Pelicula", t => t.PeliculaId, cascadeDelete: true)
                .ForeignKey("dbo.Sala", t => t.SalaId, cascadeDelete: true)
                .Index(t => t.PeliculaId)
                .Index(t => t.SalaId);
            
            CreateTable(
                "dbo.Pelicula",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Nombre = c.String(),
                        DuracionMinutos = c.Int(nullable: false),
                        Sinopsis = c.String(),
                        ImageUrl = c.String(),
                        Actores = c.String(),
                        TemaId = c.Int(),
                        Discriminator = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Tema", t => t.TemaId)
                .Index(t => t.TemaId);
            
            CreateTable(
                "dbo.Tema",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Nombre = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Usuario",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Username = c.String(),
                        Password = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Sala", "CineId", "dbo.Cine");
            DropForeignKey("dbo.Funcion", "SalaId", "dbo.Sala");
            DropForeignKey("dbo.Funcion", "PeliculaId", "dbo.Pelicula");
            DropForeignKey("dbo.Pelicula", "TemaId", "dbo.Tema");
            DropIndex("dbo.Pelicula", new[] { "TemaId" });
            DropIndex("dbo.Funcion", new[] { "SalaId" });
            DropIndex("dbo.Funcion", new[] { "PeliculaId" });
            DropIndex("dbo.Sala", new[] { "CineId" });
            DropTable("dbo.Usuario");
            DropTable("dbo.Tema");
            DropTable("dbo.Pelicula");
            DropTable("dbo.Funcion");
            DropTable("dbo.Sala");
            DropTable("dbo.Cine");
        }
    }
}
