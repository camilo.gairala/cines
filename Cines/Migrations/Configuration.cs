namespace Cines.Migrations
{
    using Cines.Models;
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<Cines.DAL.CineContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
            ContextKey = "Cines.DAL.CineContext";
        }

        protected override void Seed(Cines.DAL.CineContext context)
        {
            var temas = new List<Tema>
            {
                new Tema{Id=1,Nombre="Biografico"},
                new Tema{Id=2,Nombre="Ecologia"},
                new Tema{Id=3,Nombre="Historico"},
                new Tema{Id=4,Nombre="Salvaje"},
                new Tema{Id=5,Nombre="Sociales"},
                new Tema{Id=6,Nombre="Vida"},
            };

            temas.ForEach(t => context.Temas.AddOrUpdate(s => s.Nombre, t));

            var peliculas = new List<Pelicula>
            {
                new PeliculaDocumental{DuracionMinutos=120,Nombre="La primer pelicula de toda la historia",TemaId=1,ImageUrl="~/Recursos/Imagenes/1.jpg",
                Sinopsis="Un documental acerca de como se film� la primer pel�cula de toda la historia, que sorprendentemente es la cuarta de una saga"},

                new PeliculaDocumental{DuracionMinutos=170,Nombre="La pel�cula m�s larga de estos cines",TemaId=2,ImageUrl="~/Recursos/Imagenes/2.jpg",
                Sinopsis="Un documental que narra la filmaci�n de la ganadora como Pelicula mas larga del siglo XXI, Dark Phoenix"},

                new PeliculaComercial{DuracionMinutos=110,Nombre="Jurassic Park IX, la venganza de Rexy",Actores="Juan Saurio",ImageUrl="~/Recursos/Imagenes/3.jpg",
                Sinopsis="Rexy busca enfrentarse a un mundo que no lo acepta, mientras debe superar sus propios traumas infantiles"},

                new PeliculaComercial{DuracionMinutos=110,Nombre="Wolverine",Actores="Hugh Jackman",ImageUrl="~/Recursos/Imagenes/4.jpg",
                Sinopsis="Garras"},

                new PeliculaDocumental{DuracionMinutos=170,Nombre="Trabajo pr�ctico",TemaId=6,ImageUrl="~/Recursos/Imagenes/tp.jpg",
                Sinopsis="Documental acerca de un trabajo pr�ctico"},

                new PeliculaDocumental{DuracionMinutos=170,Nombre="Harry Potter",TemaId=5,ImageUrl="~/Recursos/Imagenes/5.jpg",
                Sinopsis="Sitcom acerca de un colegio donde los estudiantes no se comportan como deber�an"},

                new PeliculaDocumental{DuracionMinutos=120,Nombre="Planeta Rojo",TemaId=2,ImageUrl="~/Recursos/Imagenes/Planeta-rojo.jpg",
                Sinopsis="Un planeta donde la gente es dalt�nica"},

                new PeliculaComercial{DuracionMinutos=110,Nombre="Anteojos",Actores="Stormtrooper#71",ImageUrl="~/Recursos/Imagenes/lentes.jpg",
                Sinopsis="Un clon que encuentra una forma de mejorar la punter�a propia y de todos sus compa�eros"},

                new PeliculaComercial{DuracionMinutos=135,Nombre="Spiderman malo",Actores="Peter Notparker",ImageUrl="~/Recursos/Imagenes/Venom.jpg",
                Sinopsis=""},

                new PeliculaDocumental{DuracionMinutos=120,Nombre="La dura vida en las alcantarillas",TemaId=4,ImageUrl="~/Recursos/Imagenes/it.jpg",
                Sinopsis=""}
            };

            peliculas.ForEach(p => context.Peliculas.AddOrUpdate(s => s.Nombre,p));
            context.SaveChanges();

            var cines = new List<Cine>
            {
                new Cine{Nombre = "Cine principiante", Salas=new List<Sala>
                    {
                        new SalaNormal{Numero = 1},
                        new SalaNormal{Numero = 2},
                        new SalaHD{Numero = 3},
                        new SalaHD{Numero = 4},
                        new SalaHD{Numero = 5},
                    }

                },

                new Cine{Nombre = "Cine no HD", Salas=new List<Sala>
                    {
                        new SalaNormal{Numero = 1},
                        new SalaNormal{Numero = 2},
                        new SalaNormal{Numero = 3},
                        new SalaNormal{Numero = 4},
                        new SalaNormal{Numero = 5},
                    }
                },

                new Cine{Nombre="Full HD Cinema", Salas=new List<Sala>
                    {
                        new SalaHD{Numero = 1},
                        new SalaHD{Numero = 2},
                        new SalaHD{Numero = 3},
                        new SalaHD{Numero = 4},
                        new SalaHD{Numero = 5},
                    }
                },

                new Cine{Nombre="Cine Monumento",Salas=new List<Sala>
                    {
                        new SalaNormal{Numero = 1},
                        new SalaNormal{Numero = 2},
                        new SalaHD{Numero = 3},
                        new SalaHD{Numero = 4},
                        new SalaHD{Numero = 5},
                    }

                },

                new Cine{Nombre="Cine Calle 24",Salas=new List<Sala>
                    {
                        new SalaNormal{Numero = 1},
                        new SalaNormal{Numero = 2},
                        new SalaHD{Numero = 3},
                        new SalaNormal{Numero = 4},
                        new SalaHD{Numero = 5},
                    }

                },
            };

            cines.ForEach(c => context.Cines.AddOrUpdate(s => s.Nombre, c));

            var usuarios = new List<Usuario>
            {
                new Usuario{Username="admin",Password="admin"}
            };
            usuarios.ForEach(u => context.Usuarios.AddOrUpdate(s => s.Username, u));
            

            context.SaveChanges();
        }
    }
}
