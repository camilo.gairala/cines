﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Cines.Models
{
    public class Cine
    {
        public int Id { get; set; }
        public string Nombre { get; set; }

        public List<Sala> Salas { get; set; }

        //Por algún motivo no pude hacer que me funcionara con entity, siempre me traia las salas vacías, así que tuve que recurrir a esto
        public List<Sala> traerSalas()
        {
            return this.Salas;
        }
    }
}