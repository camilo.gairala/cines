﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Cines.Models
{
    public class Tema
    {
        public int Id { get; set; }

        public string Nombre { get; set; }
    }
}