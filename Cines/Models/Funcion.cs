﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Cines.Models
{
    public class Funcion
    {
        public int Id { get; set; }

        [ForeignKey("Pelicula")]
        public int PeliculaId { get; set; }
        public Pelicula Pelicula { get; set; }

        [ForeignKey("Sala")]
        public int SalaId { get; set; }
        public Sala Sala { get; set; }

        //[DataType(DataType.Date)]
        public DateTime Horario { get; set; }
    }
}