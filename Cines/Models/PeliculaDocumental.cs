﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;



namespace Cines.Models
{
    public class PeliculaDocumental : Pelicula
    {
        [ForeignKey("Tema")]
        public int? TemaId { get; set; }
        public Tema Tema { get; set; }

    }

}