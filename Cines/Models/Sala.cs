﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Cines.Models
{
    public abstract class Sala
    {
        public int Id { get; set; }
        public int Numero { get; set; }

        //Tuve que poner esto también porque entity no me traía los cines cargados con salas
        public int CineId { get; set; }

        public List<Funcion> Funciones { get; set; }

        [NotMapped]
        public string MyDiscriminator
        {
            get {
                var nombre = this.GetType().Name;
                nombre = nombre.Remove(0, 4);
                return nombre;
            }
        }
    }
}