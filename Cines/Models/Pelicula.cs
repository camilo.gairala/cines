﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Cines.Models
{
    public abstract class Pelicula
    {
        public int ID { get; set; }
        public string Nombre { get; set; }
        public int DuracionMinutos { get; set; }

        public string Sinopsis { get; set; }

        public string ImageUrl { get; set; }

        [NotMapped]
        public HttpPostedFileBase Imagen { get; set; }

        [NotMapped]
        public string MyDiscriminator
        {
            get{ return this.GetType().Name; }
        }

    }
}