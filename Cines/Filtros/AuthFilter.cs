﻿using Cines.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;


namespace Cines.Filtros
{
    public class AuthFilter : ActionFilterAttribute, IAuthorizationFilter
    {
        public void OnAuthorization(AuthorizationContext filterContext)
        {
            var usuario = (Usuario)filterContext.HttpContext.Session["Usuario"];
            if (usuario == null)
            {
                filterContext.Result = new RedirectToRouteResult(
                    new RouteValueDictionary { { "Controller", "Peliculas" }, { "Action", "Index" } }
                );
            }

        }

    }
}